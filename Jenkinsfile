properties(
	[
pipelineTriggers([parameterizedCron('''TZ=Asia/Kolkata 
        00 08 * * 2,5 %executionEnv=Prod US;suite=Regression;module=All
        00 05 * * 2 %executionEnv=Prod US;suite=Delete;module=Delete;
		00 05 * * 2 %executionEnv=Prod IN;suite=Delete;module=Delete
		00 05 * * 5 %executionEnv=Staging;suite=Delete;module=Delete
        ''')]),
	parameters([
		[
			$class: 'ChoiceParameter', 
			choiceType: 'PT_RADIO', 
			filterLength: 1, 
			filterable: false, 
			name: 'suite', 
			randomName: 'choice-parameter-453857694696436', 
			script: [
				$class: 'GroovyScript', 
				fallbackScript: [classpath: [], sandbox: false, script: ''], 
				script: [
					classpath: [], 
					sandbox: false, 
					script: '''return[
					\'Regression:selected\',
					\'Delete\'
					]'''
				]
			]
		], 
		[
			$class: 'CascadeChoiceParameter', 
			choiceType: 'PT_CHECKBOX', 
			filterLength: 1, 
			filterable: false, 
			name: 'module', 
			randomName: 'choice-parameter-454966654763424', 
			referencedParameters: 'suite', 
			script: [
				$class: 'GroovyScript', 
				fallbackScript: [classpath: [], sandbox: false, script: ''], 
				script: [
				classpath: [], 
					sandbox: false, 
					script: '''if(suite.equals("Regression")){
					return ['Segments:selected','Integration:selected','Campaigns:selected']
					}else{
					return ['Caution! You are about to delete data:disabled','Delete Journeys','Delete WA Campaigns','Delete SMS Campaigns','Delete Email Campaigns','Delete WebPush Campaigns','Delete InLine Content','Delete Push Campaigns','Delete InApp Content','Delete OnSite Notif','Delete Segments','Delete Static Lists','Delete SSP','Delete ESP','Delete WSP','Delete InLineApp Content','Delete WSP Templates','Delete Catalogs','Delete Recommendations']
					}'''	
				]
			]
		],
		[
		$class: 'ChoiceParameter', 
		choiceType: 'PT_RADIO', 
		filterLength: 1, 
		filterable: false, 
		name: 'executionEnv', 
		randomName: 'choice-parameter-457227329686590', 
		script: [
			$class: 'GroovyScript', 
			fallbackScript: [classpath: [], sandbox: false, script: ''], 
			script: [
				classpath: [], 
				sandbox: false, 
				script: '''return[
				\'Prod US\',
				\'Prod IN\',
				\'Prod Beta\',
				\'Staging Dev\',
				\'Staging:selected\'
				]'''
			]
		]
	]
	])
])
namespacePrompt='default'
def accountName=getAccountName(params.executionEnv)
colorValue="color"
slackExecutionMessage="message"
boolean scheduledTrigger=!currentBuild.getBuildCauses('org.jenkinsci.plugins.parameterizedscheduler.ParameterizedTimerTriggerCause').isEmpty()
boolean remoteTrigger=!currentBuild.getBuildCauses('hudson.model.Cause$RemoteCause').isEmpty()
agentName= "windows_slave"
pipeline {
    agent none
    tools {
        maven "Maven"
    }
    options {
	    buildDiscarder(logRotator(numToKeepStr: '10'))
	    timestamps()
	    skipDefaultCheckout()
    }
    stages {
        stage('Namespace and Slave prompt') {
       	agent {label "master"}
	        when {
	            expression {!(scheduledTrigger||remoteTrigger)}
	        }
           	steps {
               script {
	               if(params.executionEnv == 'Staging'){
						def inputValue = input(id:'inputValue', message: 'Enter the namespace and the node that you desire to execute on', ok: 'Proceed',
										 parameters: [
	                                        	 string(defaultValue: 'automation',
	                                            description: 'Will run by default on automation, please change if reqd.',
	                                            name: 'namespace'),
	                                         	string(defaultValue: 'windows_slave',
	                                            description: 'Will run by default on windows_slave node, make sure the node is up and connected',
	                                            name: 'node')
	                                        ])
	                    namespacePrompt = inputValue['namespace']
	                    agentName=inputValue['node']
	                    if(params.suite == 'Delete'){
	                        def accInputValue = input(id:'accInputValue', message: 'Enter the account name that you wish to clear data from', ok: 'Proceed',
	                                    parameters: [
	                                    		string(defaultValue: 'enter value',
	                                            description: 'Will delete data for chosen acc and env',
	                                            name: 'accname')
	                                        ])
	                         accountName=accInputValue               
	                                  }
						
	               }else{
	                   def inputValue = input(id:'inputValue', message: 'Enter the node that you desire to execute on', ok: 'Proceed',
	                                    parameters: [
	                                    		string(defaultValue: 'windows_slave',
	                                            description: 'Will run by default on windows_slave, make sure the node is up and connected',
	                                            name: 'node')
	                                        ])
	                        agentName=inputValue
	                        if(params.suite == 'Delete'){
	                        def accInputValue = input(id:'accInputValue', message: 'Enter the account name that you wish to clear data from', ok: 'Proceed',
	                                    parameters: [
	                                    		string(defaultValue: '',
	                                            description: 'Will delete data for chosen acc and env',
	                                            name: 'accname')
	                                        ])
	                         accountName=accInputValue              
	                                  }
	               }
           		}
        	}
        }
        stage('Checkout'){
        	agent {label agentName}
			steps {
				checkout scm
				stash 'projectWS'
			}
		}
        stage('Trigger Type') {
	        parallel {
		        stage('Scheduled/Remote Build'){
			        agent {label agentName}
			        when {
	        			expression {scheduledTrigger || remoteTrigger}
	        		}
		        	stages{
		            	stage('Scheduled Test'){
							steps {
	            				script {
	            					unstash 'projectWS'
					           		if(isUnix()){										
					           			sh 'mvn clean compiler:compile'										
					           			sh "mvn exec:java -Dset.Environment=\"${executionEnv}\" -Dset.Suite=\"${suite}\" -Dset.Module=\"${module}\" -Dset.Namespace=automation -Dset.Account=\"${accountName}\" verify"
					           		}else{
					           			bat 'mvn clean compiler:compile'
					           			bat "mvn exec:java -Dset.Environment=\"${executionEnv}\" -Dset.Suite=\"${suite}\" -Dset.Module=\"${module}\" -Dset.Namespace=automation -Dset.Account=\"${accountName}\" verify"
				           			}
	           					}
							}
							post {
					    		always {
					    			postExecutionScript(scheduledTrigger,remoteTrigger)
					    		}
					    		success {
		    					    script {
		    					    	if(remoteTrigger){
		    					    		echo "Remote Trigger Build successful"
		    					    	}
    					    		}
		    					}
							}
		            	}
		        	}
		        }
				stage('User Triggered Build'){
				 	agent {label agentName}
				 	when {
        		    	expression {!(scheduledTrigger || remoteTrigger)}
        			}
					stages{
		             	stage('User triggered Test'){
							steps {
	            				script {
	            					unstash 'projectWS'
		            				if(isUnix()){
						            	sh 'mvn clean compiler:compile'
					           			sh "mvn exec:java -Dset.Environment=\"${executionEnv}\" -Dset.Suite=\"${suite}\" -Dset.Module=\"${module}\" -Dset.Namespace=\"${namespacePrompt}\" -Dset.Account=\"${accountName}\" verify"
		            				}else{
						            	bat 'mvn clean compiler:compile'
						            	bat "mvn exec:java -Dset.Environment=\"${executionEnv}\" -Dset.Suite=\"${suite}\" -Dset.Module=\"${module}\" -Dset.Namespace=\"${namespacePrompt}\" -Dset.Account=\"${accountName}\" verify"    
		            				}
								}
	        				}
	        				post {
	        	    			always {
	        	    				postExecutionScript(false,false)
	        	    			}
	        				}
		             	}
		            }
				}
           	}
        }
    }
}

def sendSlackMessage(scheduled,jobDetails,jobStatus,envStatus){
	buildParameters=""
    pipelineDetails="<https://jenkins.stg.webengage.biz/blue/organizations/jenkins/API%20Automation/detail/API%20Automation/${env.BUILD_NUMBER}/pipeline|*View Build Details*>"
	serenityReports="<https://jenkins.stg.webengage.biz/job/API%20Automation/${env.BUILD_NUMBER}/Serenity_20Reports/|*View Execution Reports*>"
	quickGlance="<https://jenkins.stg.webengage.biz/blue/organizations/jenkins/API%20Automation/detail/API%20Automation/${env.BUILD_NUMBER}/tests|Quick glance at all the tests>"
	slackMsg="${jobDetails}\n${jobStatus}\n${envStatus}\n${pipelineDetails}\n${serenityReports}"
	if(!scheduled){
		buildParameters="\n<https://jenkins.stg.webengage.biz/job/API%20Automation/${env.BUILD_NUMBER}/parameters/|*View Build Parameters*>"
	}
	echo "${envStatus}"
	if ( currentBuild.currentResult == "SUCCESS" ) {
		customMsg="*Things are in place!* :+1:"
		slackExecutionMessage="${slackMsg}${buildParameters}\n_${quickGlance}_\n\n${customMsg}";
		colorValue='#00f61d';
	}else{
		customMsg="*Things went haywire! :boom: Please have a look.* "
		slackExecutionMessage="${slackMsg}${buildParameters}\n_${quickGlance}_\n\n${customMsg}";
		colorValue='#df1019';
	}
}

def namespace(namespaceValue){
    if(!namespaceValue.contains("default")){
        return " - "+namespaceValue
    }else{
		return ""
    }
}

def postExecutionScript(scheduled,remote){
	script {
		jobDetails = "Job : *${env.JOB_NAME}* - *${suite}*"
		jobDetails = remote ? jobDetails+" | Remote CICD trigger": jobDetails			
		jobStatus = "Status : *${currentBuild.currentResult}*"
		envStatus = "Environment : *${executionEnv}*"+namespace("${namespacePrompt}")
		sendSlackMessage(scheduled,jobDetails,jobStatus,envStatus)
	}
	cucumber failedFeaturesNumber: -1, failedScenariosNumber: -1, failedStepsNumber: -1, fileIncludePattern: '**/Cucumber.json', pendingStepsNumber: -1, skippedStepsNumber: -1, sortingMethod: 'ALPHABETICAL', undefinedStepsNumber: -1
	publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'target/site/serenity', reportFiles: 'index.html', reportName: 'Serenity Reports', reportTitles: ''])
	script{
		def summary = junit testResults: 'target\\surefire-reports\\*.xml'
		def passed = "${summary.passCount}" as Double
		def failed = "${summary.failCount}" as Double 
		def percentage=((passed/(passed+failed))*100).round(2)
		def testSummary="\n\n\n*Test Summary* \n Total: ${summary.totalCount}; Failures: ${summary.failCount}; Skipped: ${summary.skipCount}; Success %: ${percentage}"
		slackSend color:"${colorValue}", message: "${slackExecutionMessage}${testSummary}", channel: 'testautomation'
	}

}
def getAccountName(executionEnv){
	switch(executionEnv) {
                        case "Prod US": 
							accountName="UIAutomation"; 
							break
                        case "Prod IN": 
							accountName="UIAutomationIndia"; 
							break
                        case "Staging": 
							accountName="UIAutomationStaging"; 
							break
                    }
	return accountName
}
