package com.webengage.api_automation.utilities;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;

import com.webengage.api_automation.pojopackages.user.Attributes;

import io.cucumber.messages.internal.com.google.gson.Gson;
import io.restassured.response.Response;

public class UserModuleUtility {
	CommonUtility commonUtility = new CommonUtility();
	Response response = CommonUtility.response;

	public String createSingleUser(HashMap<String, HashMap<String, String>> data) throws Exception {
		Iterator<String> itr = data.keySet().iterator();

		String className = itr.next();
		Class<?> class_Name = Class.forName("com.webengage.api_automation.pojopackages.user." + className);
		Object obj1 = class_Name.newInstance();
		obj1 = commonUtility.methodInvocation(data, className, obj1);

		className = itr.next();
		class_Name = Class.forName("com.webengage.api_automation.pojopackages.user." + className);
		Object obj2 = class_Name.newInstance();
		obj2 = commonUtility.methodInvocation(data, className, obj2);

		Method m = obj1.getClass().getMethod("setAttributes",Attributes.class);
		m.invoke(obj1, obj2);
		return new Gson().toJson(obj1).replaceAll("_", " ");
	}

}