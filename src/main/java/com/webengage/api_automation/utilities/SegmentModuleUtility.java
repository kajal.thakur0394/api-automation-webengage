package com.webengage.api_automation.utilities;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

public class SegmentModuleUtility {
	LinkedList<LinkedList<String>> runtimeURL = new LinkedList<>();
	public static LinkedHashMap<String, String> runtimeBody = new LinkedHashMap<>();
	LinkedList<LinkedList<String>> runtimeQueryParams = new LinkedList<>();

	public void setDynamicRequest(String runtimeKey, String type) {
		LinkedList<String> runtimeList = new LinkedList<>();
		String value = CommonUtility.runtimeValues.get(runtimeKey);
		String replaceWord = "{" + runtimeKey.split("\\.")[1] + "}";
		replaceWord=replaceWord.replaceAll("[0-9]", "");
		runtimeList.add(replaceWord);
		runtimeList.add(value);
		switch (type) {
		case "URL":
			runtimeURL.add(runtimeList);
			break;
		case "Body":
			runtimeBody.put(replaceWord, value);
			break;
		case "Query":
			runtimeQueryParams.add(runtimeList);
			break;
		default:
			break;
		}
	}

	public LinkedList<LinkedList<String>> getDynamicURL() {
		return runtimeURL;
	}

	public LinkedHashMap<String, String> getDynamicBody() {
		return runtimeBody;
	}

	public LinkedList<LinkedList<String>> getDynamicQueryParams() {
		return runtimeQueryParams;
	}

	public void setDynamicRequestLicenseCode(String type, String value) {
		LinkedList<String> runtimeList = new LinkedList<>();
		runtimeList.add("{licenseCode}");
		runtimeList.add(value);
		switch (type) {
		case "URL":
			runtimeURL.add(runtimeList);
			break;
		case "Body":
			runtimeBody.put("{licenseCode}", value);
			break;
		case "Query":
			runtimeQueryParams.add(runtimeList);
			break;
		default:
			break;
		}

	}

	public String modifyAPIURL(String apiURI) {
		if (getDynamicURL() != null) {
			for (LinkedList<String> list : getDynamicURL()) {
				String replaceKeyword = list.get(0);
				String value = list.get(1);
				apiURI = apiURI.replace(replaceKeyword, value);
			}
			return apiURI;
		} else {
			return apiURI;
		}
	}

	public String modifyBody(String jsonBody) {
		if (getDynamicBody() != null) {
			for (Entry<String, String> element : getDynamicBody().entrySet()) {
				String replaceKeyword = element.getKey();
				String value = element.getValue();
				try {
					jsonBody = jsonBody.replace(replaceKeyword, value);
				} catch (Exception e) {
					System.out.println("There is no replacement value present for "+replaceKeyword);
				}
				
			}
			return jsonBody;
		} else {
			return jsonBody;
		}
	}

	public String modifyQueryParams(String queryParamsString) {
		if (getDynamicQueryParams() != null) {
			for (LinkedList<String> list : getDynamicQueryParams()) {
				String replaceKeyword = list.get(0);
				String value = list.get(1);
				queryParamsString = queryParamsString.replace(replaceKeyword, value);
			}
			return queryParamsString;
		} else {
			return queryParamsString;
		}
	}

}