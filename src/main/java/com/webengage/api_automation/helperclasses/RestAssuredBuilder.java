package com.webengage.api_automation.helperclasses;

import java.io.IOException;
import java.util.List;

import com.webengage.api_automation.utilities.SetupUtility;

import io.restassured.RestAssured;
import io.restassured.filter.Filter;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAssuredBuilder {
	private RequestSpecification request;
	SetupUtility setupUtility = new SetupUtility();
	private String method;
	private String apiURI;

	public RestAssuredBuilder(String apiURI, String method) throws IOException {
		this.method = method;
		this.apiURI = apiURI;
		RestAssured.baseURI = setupUtility.getBaseURI();
		RestAssured.urlEncodingEnabled=false;
		request = RestAssured.given();
		request.header("Content-Type", "application/json").header("Authorization",
				"Bearer " + SetupUtility.getAuthApiKey());
	}

	public Response buildAPI() {
		Filter logFilter = new CustomLogFilter();
		switch (this.method) {
		case "POST":
			return request.filter(logFilter).post(apiURI);
		case "GET":
			return request.filter(logFilter).get(apiURI);
		case "PUT":
			return request.filter(logFilter).put(apiURI);
		case "DELETE":
			return request.filter(logFilter).delete(apiURI);
		default:
			break;
		}
		return null;
	}

	public Response executeWithBody(String jsonBody) {
		request.body(jsonBody);
		return buildAPI();
	}

	public Response executeWithQueryParams(List<List<String>> queryParams) {
		for (List<String> s : queryParams) {
			String key = s.get(0);
			String value = s.get(1).contains("\"") ? s.get(1).replaceAll("\"", "") : s.get(1);
			request.queryParam(key, value);
		}
		return buildAPI();
	}
	
	public Response executeWithBodyAndQueryParams(String jsonBody,List<List<String>> queryParams) {
		request.body(jsonBody);
		for (List<String> s : queryParams) {
			String key = s.get(0);
			String value = s.get(1).contains("\"") ? s.get(1).replaceAll("\"", "") : s.get(1);
			request.queryParam(key, value);
		}
		return buildAPI();
	}
	
	public Response execute() {
		return buildAPI();
	}
}
