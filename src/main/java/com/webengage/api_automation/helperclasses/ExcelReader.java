package com.webengage.api_automation.helperclasses;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.webengage.api_automation.utilities.SetupUtility;

public class ExcelReader {
	public static String directoryLoc = System.getProperty("user.dir");
	public static String testDataexcel = directoryLoc + "/src/test/resources/testData/testData.xlsx";
	public static int rowNum;
	public static XSSFWorkbook valueWorkbook;
	public static XSSFSheet runtimeValuesSheet;

	@SuppressWarnings("resource")
	public static XSSFSheet readFromExcelSheet(String sheetName) throws IOException {
		FileInputStream fis = new FileInputStream(testDataexcel);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		return sheet;
	}

	public HashMap<String, HashMap<String, String>> processExcelSheetRowData(XSSFSheet sheet, String userID)
			throws Exception {
		HashMap<String, HashMap<String, String>> keyMap = new HashMap<>();
		HashMap<String, String> valueMap = new HashMap<>();
		Iterator<Row> rows = sheet.rowIterator();
		String temp = "";
		Row headerRow = rows.next();
		while (rows.hasNext()) {
			Iterator<Cell> cells = rows.next().cellIterator();
			Cell identifier = cells.next();
			if (identifier.getStringCellValue().equals(userID)) {
				Iterator<Cell> headerCell = headerRow.cellIterator();
				headerCell.next();
				while (cells.hasNext()) {
					String headerCellContent = headerCell.next().getStringCellValue().toString();
					String className = headerCellContent.split(":")[0];
					String method;
					try {
						method = headerCellContent.split(":")[1] + "-" + headerCellContent.split(":")[2];
					} catch (ArrayIndexOutOfBoundsException e) {
						method = headerCellContent.split(":")[1] + "-String";
					}
					Cell value = cells.next();
					String cellValue;
					try {
						cellValue = value.getStringCellValue().replaceAll("\"", "");
					} catch (Exception e) {
						cellValue = String.valueOf(value.getNumericCellValue());
					}

					if (temp.isEmpty()) {
						valueMap.put(method, cellValue);
						keyMap.put(className, valueMap);
					} else if (temp.equals(className)) {
						valueMap.put(method, cellValue);
						keyMap.put(className, valueMap);
					} else {
						valueMap = new HashMap<>();
						valueMap.put(method, cellValue);
						keyMap.put(className, valueMap);
					}
					temp = className;
				}
			}
		}
		return keyMap;
	}

	public HashMap<String, String> processExcelSheetData(XSSFSheet sheetObj, String refId) {
		Iterator<Row> rows = sheetObj.rowIterator();
		HashMap<String, String> postReq = new HashMap<>();
		Row headerRow = rows.next();
		while (rows.hasNext()) {
			Iterator<Cell> cells = rows.next().cellIterator();
			Cell identifier = cells.next();
			if (identifier.getStringCellValue().equals(refId)) {
				Iterator<Cell> headerCell = headerRow.cellIterator();
				headerCell.next();
				while (cells.hasNext()) {
					if (headerCell.next().getStringCellValue().equals("QueryParams")) {
						postReq.put("QueryParams", cells.next().getStringCellValue());
					} else {
						postReq.put("Body", cells.next().getStringCellValue());
					}
				}
				break;
			}
		}
		return postReq;
	}

	public static void flushRuntimeParams() throws IOException {
		FileInputStream fis = new FileInputStream(testDataexcel);
		valueWorkbook = new XSSFWorkbook(fis);
		valueWorkbook.removeSheetAt(valueWorkbook.getSheetIndex("savedValues"));
		runtimeValuesSheet = valueWorkbook.createSheet("savedValues");
		rowNum=0;
		Row currentRow = runtimeValuesSheet.createRow(ExcelReader.rowNum++);
		currentRow.createCell(0).setCellValue("Scenario Name");
		currentRow.createCell(1).setCellValue("Identifier");
		currentRow.createCell(2).setCellValue("Value");
	}

	public static void writeRuntimeValues() throws IOException {
		FileOutputStream outputStream = new FileOutputStream(
				SetupUtility.directoryLoc + "/src/test/resources/testData/testData.xlsx");
		valueWorkbook.write(outputStream);
		valueWorkbook.close();
		outputStream.close();
	}

	public void saveRuntimeValues(String scName, String scTag, String valueName, String value) throws IOException {
		Row currentRow = runtimeValuesSheet.createRow(ExcelReader.rowNum++);
		currentRow.createCell(0).setCellValue(scName);
		currentRow.createCell(1).setCellValue(scTag + "." + valueName);
		currentRow.createCell(2).setCellValue(value);
	}
}
