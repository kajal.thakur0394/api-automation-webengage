package com.webengage.api_automation.pojopackages.user;

public class Attributes {

	private String age;
	private Boolean male;
	private String Twitter_username;
	private Double Dollars_spent;
	private Integer Points_earned;

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Boolean getMale() {
		return male;
	}

	public void setMale(Boolean male) {
		this.male = male;
	}

	public String getTwitterUsername() {
		return Twitter_username;
	}

	public void setTwitterUsername(String twitterUsername) {
		this.Twitter_username = twitterUsername;
	}

	public Double getDollarsSpent() {
		return Dollars_spent;
	}

	public void setDollarsSpent(Double dollarsSpent) {
		this.Dollars_spent = dollarsSpent;
	}

	public Integer getPointsEarned() {
		return Points_earned;
	}

	public void setPointsEarned(Integer pointsEarned) {
		this.Points_earned = pointsEarned;
	}

}