package com.webengage.api_automation.hooks;

import java.util.Collection;
import java.util.stream.Collectors;

import com.webengage.api_automation.helperclasses.CustomLogFilter;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

import com.webengage.api_automation.utilities.CommonUtility;

public class Hooks {
	public static String scenarioName;
	public static String uniqueTag;

	@Before
	public void beforeScenario(Scenario scenario) {
		CommonUtility.jsonBody = null;
		CommonUtility.response = null;
		CustomLogFilter.logRequest = null;
		CustomLogFilter.logResponse = null;
		uniqueTag = getScenarioTagID(scenario.getSourceTagNames());
		scenarioName = scenario.getName();
	}

	public String getScenarioTagID(Collection<String> collection) {
		try {
			return collection.stream().filter(s -> s.contains("issue")).collect(Collectors.toList()).get(0)
					.split(":")[1];
		} catch (Exception e) {
			return null;
		}

	}
}