package com.webengage.api_automation.stepDefinitions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.simple.parser.ParseException;

import com.webengage.api_automation.helperclasses.ExcelReader;
import com.webengage.api_automation.hooks.Hooks;
import com.webengage.api_automation.utilities.CommonUtility;
import com.webengage.api_automation.utilities.SegmentModuleUtility;
import com.webengage.api_automation.utilities.SetupUtility;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SegmentStepDefinitions {
	SegmentModuleUtility segmentModule = new SegmentModuleUtility();
	CommonUtility commonUtility = new CommonUtility();
	SetupUtility setupUtility = new SetupUtility();
	ExcelReader excelReader = new ExcelReader();

	@Steps
	CommonUtility commonUtilityStep;

	@And("I hit the POST method using static body for {string} api from {string} sheet with ref id {string}")
	public void i_hit_the_post_method_using_static_body_from_sheet_with_ref_id(String apiName, String sheetName,
			String refId) throws IOException, ParseException {
		setupUtility.setApiURI(apiName);
		XSSFSheet sheetObj = setupUtility.readFromExcelSheet(sheetName);
		HashMap<String, String> apiParams = excelReader.processExcelSheetData(sheetObj, refId);
		CommonUtility.jsonBody = apiParams.get("Body");
		String apiURI = setupUtility.getApiURI();
		apiURI = segmentModule.modifyAPIURL(apiURI);
		CommonUtility.jsonBody = segmentModule.modifyBody(CommonUtility.jsonBody);
		String queryParamsString = apiParams.get("QueryParams");
		queryParamsString = segmentModule.modifyQueryParams(queryParamsString);
		List<List<String>> queryParams = commonUtility.generateQueryParams(queryParamsString);
		commonUtilityStep.postRequest(CommonUtility.jsonBody, queryParams, apiURI);
	}

	@And("I hit the GET method using static body for {string} api from {string} sheet with ref id {string}")
	public void i_hit_the_get_method_using_static_body_for_api_from_sheet_with_ref_id(String apiName, String sheetName,
			String refId) throws IOException {
		setupUtility.setApiURI(apiName);
		XSSFSheet sheetObj = setupUtility.readFromExcelSheet(sheetName);
		HashMap<String, String> apiParams = excelReader.processExcelSheetData(sheetObj, refId);
		String queryParamsString = apiParams.get("QueryParams");
		queryParamsString = segmentModule.modifyQueryParams(queryParamsString);
		List<List<String>> queryParams = commonUtility.generateQueryParams(queryParamsString);
		String apiURI = setupUtility.getApiURI();
		apiURI = segmentModule.modifyAPIURL(apiURI);
		commonUtilityStep.getRequest(apiURI, queryParams);
	}

	@And("I save {string} from response as {string}")
	public void i_save_from_response_as(String jsonPath, String valueName) throws IOException {
		String scName = Hooks.scenarioName;
		String scTag = Hooks.uniqueTag;
		String value = commonUtility.fetchValue(jsonPath);
		excelReader.saveRuntimeValues(scName, scTag, valueName, value);
		CommonUtility.runtimeValues.put(scTag + "." + valueName, value);
	}

	@And("I use value of {string} for dynamic URL")
	public void i_use_value_of_for_dynamic_url(String runtimeKey) {
		segmentModule.setDynamicRequest(runtimeKey, "URL");
	}

	@And("I use value of {string} for dynamic body")
	public void i_use_value_of_for_dynamic_body(String runtimeKey) {
		segmentModule.setDynamicRequest(runtimeKey, "Body");
	}

	@And("I use value of {string} for dynamic Query Parameters")
	public void i_use_value_of_for_dynamic_queryParams(String runtimeKey) {
		segmentModule.setDynamicRequest(runtimeKey, "Query");
	}

	@And("I use license code for dynamic {string}")
	public void i_use_license_code_for_dynamic_body(String type) {
		String value = setupUtility.getLicenseCode();
		segmentModule.setDynamicRequestLicenseCode(type, value);
	}

	@And("I hit the PUT method using static body for {string} api from {string} sheet with ref id {string}")
	public void i_hit_the_put_method_using_static_body_from_sheet_with_ref_id(String apiName, String sheetName,
			String refId) throws IOException, ParseException {
		setupUtility.setApiURI(apiName);
		XSSFSheet sheetObj = setupUtility.readFromExcelSheet(sheetName);
		HashMap<String, String> apiParams = excelReader.processExcelSheetData(sheetObj, refId);
		CommonUtility.jsonBody = apiParams.get("Body");
		String apiURI = setupUtility.getApiURI();
		apiURI = segmentModule.modifyAPIURL(apiURI);
		CommonUtility.jsonBody = segmentModule.modifyBody(CommonUtility.jsonBody);
		String queryParamsString = apiParams.get("QueryParams");
		queryParamsString = segmentModule.modifyQueryParams(queryParamsString);
		List<List<String>> queryParams = commonUtility.generateQueryParams(queryParamsString);
		commonUtilityStep.putRequest(CommonUtility.jsonBody, queryParams, apiURI);
	}

	@When("I hit the PUT method for {string} api")
	public void i_hit_the_put_method_for_api(String apiName) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = segmentModule.modifyAPIURL(apiURI);
		commonUtilityStep.putRequest(apiURI);
	}
}