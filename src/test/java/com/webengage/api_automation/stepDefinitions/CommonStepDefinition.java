package com.webengage.api_automation.stepDefinitions;

import java.io.IOException;
import java.util.List;
import com.webengage.api_automation.utilities.CommonUtility;
import com.webengage.api_automation.utilities.SetupUtility;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CommonStepDefinition {
	SetupUtility setupUtility = new SetupUtility();
	CommonUtility commonUtility = new CommonUtility();

	@Given("I want to perform actions in {string} project")
	public void i_want_to_perform_actions_in_project(String projectName) throws Exception {
		setupUtility.loadCredentials(projectName);
	}

	@Steps
	CommonUtility commonUtilityStep;

	@When("I hit the post method for {string} api")
	public void i_hit_the_post_method_for_api(String apiName) throws IOException {
		setupUtility.setApiURI(apiName);
		commonUtilityStep.postRequest(CommonUtility.jsonBody, setupUtility.getApiURI());
	}

	@Then("verify the response status code as {int}")
	public void verify_the_response_status_code_for(int responseCode) {
		commonUtility.verifyStatusCode(responseCode);
	}

	@And("verify for {string} value as {string} in response")
	public void verify_for_value_as(String jsonPath, String value) {
		if (jsonPath.contains("contents[i]"))
			commonUtility.verifyValuefromArray(jsonPath, value);
		else
			commonUtility.verifyValue(jsonPath, value);

	}

	@And("I hit the GET method for {string} api using following Query Parameters")
	public void i_hit_the_get_method_for_api_using_following_query_parameters(String apiName, DataTable dataTable)
			throws IOException {
		setupUtility.setApiURI(apiName);
		List<List<String>> queryParams = dataTable.asLists(String.class);
		commonUtilityStep.getRequest(setupUtility.getApiURI(), queryParams);
	}

	@Then("verify for {string} value as {string} from saved runtime values")
	public void verify_for_value_as_from_saved_runtime_options(String jsonPath, String runtimeKey) {
		if (jsonPath.contains("contents[i]"))
			commonUtility.verifyValuefromArray(jsonPath, CommonUtility.runtimeValues.get(runtimeKey));
		else
			commonUtility.verifyValue(jsonPath, CommonUtility.runtimeValues.get(runtimeKey));
	}

	@When("I hit the GET method for {string} api")
	public void i_hit_the_get_method_for_api(String apiName) throws IOException {
		setupUtility.setApiURI(apiName);
		commonUtilityStep.getRequest(setupUtility.getApiURI());
	}

}
