package com.webengage.api_automation.stepDefinitions;

import java.util.HashMap;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import com.webengage.api_automation.helperclasses.ExcelReader;
import com.webengage.api_automation.utilities.CommonUtility;
import com.webengage.api_automation.utilities.UserModuleUtility;
import com.webengage.api_automation.utilities.SetupUtility;

public class UserStepDefinitions {
	UserModuleUtility userModule = new UserModuleUtility();
	CommonUtility commonUtility = new CommonUtility();
	SetupUtility setupUtility = new SetupUtility();
	ExcelReader excelReader = new ExcelReader();

	@Steps
	CommonUtility commonUtilityStep;

	@When("I create a single user using {string} api from {string} sheet with id {string}")
	public void i_create_a_single_user_using_api_from_sheet(String apiName, String sheetName, String userId)
			throws Exception {
		XSSFSheet sheetObj = setupUtility.readFromExcelSheet(sheetName);
		HashMap<String, HashMap<String, String>> individualData = excelReader.processExcelSheetRowData(sheetObj,
				userId);
		String jsonBody = userModule.createSingleUser(individualData);
		CommonUtility.jsonBody = jsonBody;
		setupUtility.setApiURI(apiName);
		commonUtilityStep.postRequest(CommonUtility.jsonBody, setupUtility.getApiURI());
	}
}