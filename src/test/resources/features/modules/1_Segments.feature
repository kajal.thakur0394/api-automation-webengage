@Regression @Segments
Feature: Segments and User Module
  Description: Verify functioning of APIs in Segments and User Module.This feature will cover single point checks and also end to end test scenarios of Segments module

  @issue:QAL-86
  Scenario Outline: Upload a User using users API
    When I create a single user using "createUser" api from "CreateUser" sheet with id "<identifier>"
    And I hit the GET method for "fetchUser" api using following Query Parameters
      | userStatus | known          |
      | userId     | "<identifier>" |
    Then verify for "response.data.firstName" value as "<expectedValue>" in response

    Examples: 
      | identifier | expectedValue |
      | AutoID2    | Nilesh        |

  @issue:QAL-91
  Scenario: Create a Live Segment
    When I create a single user using "createUser" api from "CreateUser" sheet with id "AutoID4"
    And I hit the GET method for "fetchUser" api using following Query Parameters
      | userStatus | known     |
      | userId     | "AutoID4" |
    Then verify for "response.data.firstName" value as "Ganesh" in response
    When I hit the POST method using static body for "createSegment" api from "Segments" sheet with ref id "createLiveSeg"
    And I save "response.data.id" from response as "SegmentId"
    And I use value of "QAL-91.SegmentId" for dynamic URL
    When I hit the GET method using static body for "fetchSegments" api from "Segments" sheet with ref id "fetchLiveSegment"
    Then verify for "response.data.trafficSegmentDto.status" value as "ACTIVE" in response
    Then verify for "response.data.trafficSegmentDto.id" value as "QAL-91.SegmentId" from saved runtime values

  @issue:QAL-90
  Scenario: Verify traffic-segments on live segment listing page
    Then I hit the GET method using static body for "trafficSegments" api from "Segments" sheet with ref id "trafficSegments"
    Then verify for "response.data.contents[0].name" value as "Segment42" in response
    Then verify for "response.data.contents[0].id" value as "QAL-91.SegmentId" from saved runtime values

  @issue:QAL-87
  Scenario: Verify reachability api on opening any live segment
    And I use value of "QAL-91.SegmentId" for dynamic body
    Then I hit the POST method using static body for "reachability" api from "Segments" sheet with ref id "reachability"
    Then verify for "response.data.ALL" value as "1" in response

  @issue:QAL-89
  Scenario: Verify that count api is giving proper user count of opened segment
    And I use value of "QAL-91.SegmentId" for dynamic Query Parameters
    Then I hit the POST method using static body for "count" api from "Segments" sheet with ref id "count"
    Then verify for "response.data.stats.users.all.total" value as "1" in response

  @issue:QAL-93
  Scenario: After clicking on list of user in live segment api should return all users in the segment
    And I use value of "QAL-91.SegmentId" for dynamic Query Parameters
    Then I hit the GET method using static body for "listOfUsers" api from "Segments" sheet with ref id "listOfUsers"
    Then verify for "response.data.contents[0].userId" value as "AutoID4" in response

  @issue:QAL-88
  Scenario: Create a Static Segment
    When I create a single user using "createUser" api from "CreateUser" sheet with id "AutoID2"
    And I hit the GET method for "fetchUser" api using following Query Parameters
      | userStatus | known     |
      | userId     | "AutoID2" |
    Then verify for "response.data.firstName" value as "Nilesh" in response
    When I hit the POST method using static body for "createStaticSegment" api from "Segments" sheet with ref id "createStaticSeg"
    And I save "response.data.encodedId" from response as "StaticSegmentId"
    And I save "response.data.name" from response as "StaticSegmentName"
    When I hit the GET method using static body for "fetchStaticSegments" api from "Segments" sheet with ref id "fetchStaticSegments"
    Then verify for "response.data.contents[i].name" value as "Static Segment 1" in response
    Then verify for "response.data.contents[i].encodedId" value as "QAL-88.StaticSegmentId" from saved runtime values
