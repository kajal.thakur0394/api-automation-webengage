@Regression @Integration
Feature: Integration module
  Description: Verify functioning of APIs in Integration module. This feature will cover single point checks and also end to end test scenarios of Integration module

  @issue:QAL-72
  Scenario: Adding new SSP
    And I use license code for dynamic "Body"
    When I hit the POST method using static body for "addSSP" api from "Integration" sheet with ref id "addSSP"
    And I save "response.data" from response as "SSPId"
    When I hit the GET method for "verifySSP" api
    Then verify for "response.data[0].providerId" value as "TWILIO" in response
    Then search and verify from "response.data|id" array with value as "QAL-72.SSPId" from saved runtime values

  @issue:QAL-73
  Scenario: Editing existing SSP
    And I use license code for dynamic "Body"
    And I use value of "QAL-72.SSPId" for dynamic body
    And I use value of "QAL-72.SSPId" for dynamic URL
    When I hit the PUT method using static body for "editSSP" api from "Integration" sheet with ref id "editSSP"
    Then verify the response status code as 200
    When I hit the GET method for "verifySSP" api
    Then search and verify from "response.data|id" array with value as "QAL-72.SSPId" from saved runtime values
    Then I verify the value of "name" as "Twilio SSP edit"

  @issue:QAL-75
  Scenario: Adding new ESP
    And I use license code for dynamic "Body"
    When I hit the POST method using static body for "addESP" api from "Integration" sheet with ref id "addESP"
    Then verify the response status code as 201
    And I save "response.data" from response as "ESPId"
    When I hit the GET method for "verifyESP" api
    Then verify for "response.data[0].providerId" value as "OCTANE" in response
    Then search and verify from "response.data|id" array with value as "QAL-75.ESPId" from saved runtime values

  @issue:QAL-74
  Scenario: Edit existing ESP
    And I use license code for dynamic "Body"
    And I use value of "QAL-75.ESPId" for dynamic body
    And I use value of "QAL-75.ESPId" for dynamic URL
    When I hit the PUT method using static body for "editESP" api from "Integration" sheet with ref id "editESP"
    Then verify the response status code as 200
    When I hit the GET method for "verifyESP" api
    Then search and verify from "response.data|id" array with value as "QAL-75.ESPId" from saved runtime values
    Then I verify the value of "name" as "Auto"

  @issue:QAL-94
  Scenario: Adding new WSP
    And I use license code for dynamic "Body"
    When I hit the POST method using static body for "addWSP" api from "Integration" sheet with ref id "addWSP"
    Then verify the response status code as 201
    And I save "response.data.id" from response as "WSPId"
    When I hit the GET method for "verifyWSP" api
    Then verify for "response.data[0].providerId" value as "GUPSHUP" in response
    Then search and verify from "response.data|id" array with value as "QAL-94.WSPId" from saved runtime values

  @issue:QAL-92
  Scenario: Edit existing WSP
    And I use license code for dynamic "Body"
    And I use value of "QAL-94.WSPId" for dynamic URL
    When I hit the PUT method using static body for "editWSP" api from "Integration" sheet with ref id "editWSP"
    Then verify the response status code as 200
    When I hit the GET method for "verifyWSP" api
    Then search and verify from "response.data|id" array with value as "QAL-94.WSPId" from saved runtime values
    Then I verify the value of "name" as "Auto TC Edited"
