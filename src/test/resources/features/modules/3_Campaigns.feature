@Regression @Campaigns
Feature: Campaigns Module
  Description: Verify functioning of APIs in Campaigns module across various channels.This feature will cover single point checks and also end to end test scenarios of Campaigns module

  @issue:QAL-500
  Scenario Outline: Create a new SMS Campaign using Segments
    And I use multiple values of "<segmentId>" for dynamic parameterized segment body
    When I hit the POST method using static body for "createAudienceSMS" api from "SMSCampaigns" sheet with ref id "<createAudienceRef>"
    And I save "response.data.id" from response as "<saveAs>"
    And I use value of "<useAs>" for dynamic URL
    When I hit the POST method using static body for "scheduleSMS" api from "SMSCampaigns" sheet with ref id "<useSchedule>"
    And I use multiple values of "<sspId>" for dynamic parameterized ssp body
    When I hit the PUT method using static body for "createVariationSMS" api from "SMSCampaigns" sheet with ref id "<createVariation>"
    And I hit the PUT method for "activateSMS" api
    Then verify for "response.data.sentStatus.status" value as "Running/Upcoming" in response

    Examples: 
      | segmentId                           | createAudienceRef | useSchedule    | sspId            | createVariation | saveAs      | useAs               |
      | DP1.SegmentId                       | createAudience    | createSchedule | DP1.SSPId        | variation1      | CampaignId1 | QAL-500.CampaignId1 |
      | Segment42                           | createAudience    | createSchedule | SMS Campaign SSP | variation1      | CampaignId2 | QAL-500.CampaignId2 |
      | Segment42,DND Segment,DP1.SegmentId | createAudience    | createSchedule | SMS Campaign SSP | variation1      | CampaignId3 | QAL-500.CampaignId3 |

  @issue:QAL-501
  Scenario Outline: Create a new Push Campaign using Segments
    And I use multiple values of "<segmentId>" for dynamic parameterized segment body
    And I select "<androidPackage>" for Android and "<bundleName>" for iOS Configs
    When I hit the POST method using static body for "createAudiencePush" api from "PushCampaigns" sheet with ref id "<createAudienceRef>"
    And I save "response.data.id" from response as "<saveAs>"
    And I use value of "<useAs>" for dynamic URL
    When I hit the POST method using static body for "schedulePush" api from "PushCampaigns" sheet with ref id "<useSchedule>"
    When I hit the PUT method using static body for "createVariationPush" api from "PushCampaigns" sheet with ref id "<createVariation>"
    And I hit the PUT method for "activatePush" api
    Then verify for "response.data.sentStatus.status" value as "Running/Upcoming" in response

    Examples: 
      | segmentId     | androidPackage           | bundleName               | createAudienceRef    | useSchedule    | createVariation | saveAs      | useAs               |
      | DP2.SegmentId | com.webengage.production |                          | createAudeinceSingle | createSchedule | variation1      | CampaignId1 | QAL-501.CampaignId1 |
      | DP2.SegmentId |                          | com.webEngage.{env}Swift | createAudeinceSingle | createSchedule | variation1      | CampaignId2 | QAL-501.CampaignId2 |
      | DP2.SegmentId | com.webengage.production | com.webEngage.{env}Swift | createAudeinceSingle | createSchedule | variation1      | CampaignId3 | QAL-501.CampaignId3 |

  @issue:QAL-502
  Scenario Outline: Create a new In App Notification Campaign using Segments
    And I use multiple values of "<segmentId>" for dynamic parameterized segment body
    And I select "<androidPackage>" for Android and "<bundleName>" for iOS Configs
    When I hit the POST method using static body for "createAudienceInApp" api from "inAppCampaign" sheet with ref id "<createAudienceRef>"
    And I save "response.data.id" from response as "<saveAs>"
    And I use value of "<useAs>" for dynamic URL
    And I set the Epoch time as "current time"
    When I hit the POST method using static body for "scheduleInApp" api from "inAppCampaign" sheet with ref id "<useSchedule>"
    When I hit the PUT method using static body for "createVariationsInApp" api from "inAppCampaign" sheet with ref id "<createVariation>"
    And I hit the PUT method for "activateInApp" api
    Then verify for "response.data.sentStatus.status" value as "Running/Upcoming" in response

    Examples: 
      | segmentId     | androidPackage           | bundleName               | createAudienceRef | useSchedule    | createVariation | saveAs      | useAs               |
      | DP2.SegmentId | com.webengage.production | com.webEngage.{env}Swift | createAudeince    | createSchedule | variation1      | CampaignId1 | QAL-502.CampaignId1 |

  @issue:QAL-503
  Scenario Outline: Create a new WhatsApp Campaign
    When I hit the POST method using static body for "createAudienceWA" api from "WhatsAppCampaign" sheet with ref id "<createAudienceRef>"
    And I save "response.data.id" from response as "<saveAs>"
    And I use value of "<useAs>" for dynamic URL
    And I get Credential and WSP Id for template "Hello WA"
    When I hit the POST method using static body for "scheduleWA" api from "WhatsAppCampaign" sheet with ref id "<useSchedule>"
    When I hit the PUT method using static body for "createVariationWA" api from "WhatsAppCampaign" sheet with ref id "<createVariation>"
    And I hit the PUT method for "activateWA" api
    Then verify for "response.data.sentStatus.status" value as "Running/Upcoming" in response

    Examples: 
      | createAudienceRef | useSchedule    | createVariation | saveAs      | useAs               |
      | createAudience    | createSchedule | selectTemplate  | CampaignId1 | QAL-503.CampaignId1 |
