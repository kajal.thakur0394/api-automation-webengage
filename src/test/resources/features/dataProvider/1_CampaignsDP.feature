@DataProvider
Feature: Data Provider for Campaigns module
  Description: Creates all necessary data for Campaign creation

  @issue:DP1
  Scenario: Data Provider for SMS Campaign
    Given I want to perform actions in "APIAutomation" project
    When I hit the POST method using static body for "createSegment" api from "Segments" sheet with ref id "createLiveSegDP"
    And I save "response.data.id" from response as "SegmentId"
    And I use license code for dynamic "Body"
    When I hit the POST method using static body for "addSSP" api from "Integration" sheet with ref id "addSSP"
    And I save "response.data" from response as "SSPId"

  @issue:DP2
  Scenario: Data Provider for Push Campaign
    Given I want to perform actions in "APIAutomation" project
    When I hit the POST method using static body for "createSegment" api from "Segments" sheet with ref id "createLiveSegMobile"
    And I save "response.data.id" from response as "SegmentId"
